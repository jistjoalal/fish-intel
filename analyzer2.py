from PyPDF2 import PdfFileReader
from tabula import wrapper

from helpers import loadJSON, isFound


HEADERS = [  # 2nd words
    'FACILITY', 'STOCK-BO', 'TOTAL', 'TOTAL', 'EGGTAKE', 'ADULTS', 'JACKS',
    'SPAWNED', 'SPAWNED', 'RELEASED', 'SHIPPED', 'MORTALITY', 'SURPLUS',
    'DATE', 'COMMENTS'
    ]
HEADERS_FIRST = [  # 1st words
     '', '', 'ADULT', 'JACK', 'TOTAL', 'ON HAND', 'ON HAND', 'LETHAL', 'LIVE',
     '', 'LIVE', '', '', '', ''
    ]
CHAR_WIDTH = 4


# data[page][row][col]
def parsePDF(path, a, c):
    print('\nParsing ' + path + '\n')
    raw = wrapper.read_pdf(path,
                           output_format='json',
                           guess=False,
                           area=a,
                           columns=c,
                           pages='all')
    data = []
    for page in raw:
        page_txt = []
        for row in page['data']:
            row_txt = []
            for col in row:
                text = col['text']
                row_txt.append(text.upper())
            page_txt.append(row_txt)
        data.append(page_txt)
    return data


def getArea(pdfName):
    pdf = PdfFileReader(open(pdfName, 'rb'))
    return list(pdf.getPage(0).mediaBox)


def getCols(width, numHeaders):
    cols = [width * (i / numHeaders) for i in range(numHeaders)]
    shift = width/numHeaders/2
    return [cols[i] + shift for i in range(numHeaders)][:-1]


def getNewCols(data, startCols):
    newCols = []
    headersFound = 0
    headers = []
    for page in data:
        for row in page:
            if any(header in row for header in HEADERS):
                print(row)
                headerString = ''
                for i in range(len(row)):
                    cut = row[i].split(' ')
                    if row[i] in HEADERS and row[i] != '':
                        # print(row[i])
                        headers.append(row[i])
                        headersFound += 1
                        # complete header found
                        # (HEADER1)
                        # no need to append last col since it will be defined
                        # by the column before it (COL1 [seperator1] COL2 ...)
                        if i != len(startCols):
                            newCols.append(startCols[i])
                    elif len(cut) == 2:
                        headers.append(headerString + cut[0])
                        headersFound += 1
                        # tail end of header found
                        # [headerString](DER1 ...)
                        if i != len(startCols):
                            shift = len(headerString) * CHAR_WIDTH
                            newCols.append(startCols[i] - shift)
                        if cut[1] in HEADERS:
                            headers.append(cut[1])
                            headersFound += 1
                            headerString = ''
                            # complete header found
                            # (... HEADER2)
                            if i != len(startCols):
                                shift = len(cut[0]) * CHAR_WIDTH
                                newCols.append(startCols[i] + shift)
                        else:
                            headerString = cut[1]
                    elif len(cut) == 3:
                        headers.append(headerString + cut[0])
                        headersFound += 1
                        # tail end of header found
                        # [headerString](DER1 ...)
                        shift = len(headerString) * CHAR_WIDTH
                        newCols.append(startCols[i] - shift)
                        if cut[1] in HEADERS:
                            headers.append(cut[1])
                            headersFound += 1
                            headerString = cut[2]
                            # complete header found
                            # (... HEADER1 ...)
                            shift = len(cut[0]) * CHAR_WIDTH
                            newCols.append(startCols[i] + shift)
                        else:
                            print("fail")
                    else:
                        # print(row[i])
                        headerString += row[i]

                return newCols


def reparse(filename, tries):
    area = getArea(filename)
    width = area[3] - area[1]

    freshCols = getCols(width, len(HEADERS))
    print(len(freshCols))
    reparseTry(filename, area, freshCols, tries)


def reparseTry(filename, area, oldCols, tries):
    data = parsePDF(filename, area, oldCols)
    newCols = getNewCols(data, oldCols)
    if tries > 0:
        reparseTry(filename, area, newCols, tries-1)


filename = 'data/hatchery_reports/2018/010418.pdf'
reparse(filename, 3)
