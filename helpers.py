import urllib.request
from urllib.error import HTTPError, URLError
from datetime import datetime
import time
import json
import os


# used for syncing pdf_parser and json_analyzer while troubleshooting
# parsing errors.
YEARS = {
    'start': 2018,
    'end': 2019
}


# returns json from file as hash
def loadJSON(file):
    with open(file) as f:
        return json.load(f)


# True if any needle found in any haystack
# - searches each haystack (string) for each needle (string)
# - returns true when a needle is substring of a haystack
def isFound(haystacks, needles):
    for i in haystacks:
        if any(j in i for j in needles):
            return True
    return False


# returns a string percentage from integer part and whole
# e.g. 23.52%
def successPercent(part, whole):
    return str(round(100*(part / whole), 2)) + '%'


# returns T/F based on download success
def downloadFile(url, output):
    try:
        urllib.request.urlretrieve(url, output)
    except HTTPError as e:
        print('Error code: ', e.code)
        return False
    except URLError as e:
        print('Error code: ', e.code)
        return False
    else:
        print('Hit!')
        return True


# returns unix time from date
def dateToUnix(date):
    return time.mktime(datetime.strptime(date, "%m/%d/%y").timetuple())


def stringToDate(string):
    return datetime.strptime(string, '%m/%d/%y')


# for tracking how long code takes to run
class TimeTracker:
    def __init__(self):
        self.start = time.time()

    def elapsedTime(self):
        elapsed = round(time.time() - self.start, 2)
        print('\nElapsed Time:\t' + str(elapsed) + 's')
