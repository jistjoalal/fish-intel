
from json_analyzer import Analyzer

from helpers import dateToUnix, TimeTracker, stringToDate


# sets of hatcheries to search for
HATCHERY_LISTS = {
    'HATCHERIES': [
        'WHATCOM CR HATCHERY', 'SAMISH HATCHERY', 'TOKUL CR HATCHERY',
        'GEORGE ADAMS HATCHRY', 'HOODSPORT HATCHERY', 'ISSAQUAH HATCHERY',
        'CEDAR RIVER HATCHERY', 'SOOS CREEK HATCHERY', 'VOIGHTS CR HATCHERY',
        'GARRISON HATCHERY', 'MINTER CR HATCHERY', 'GLENWOOD SPRINGS',
        'MORSE CREEK HATCHERY', 'ELWHA HATCHERY', 'HUMPTULIPS HATCHERY',
        'MAYR BROTHERS REARIN', 'LK ABERDEEN HATCHERY', 'WYNOOCHEE R DAM TRAP',
        'SATSOP SPRINGS PONDS', 'FORKS CREEK HATCHERY', 'NEMAH HATCHERY',
        'NASELLE HATCHERY', 'GRAYS RIVER HATCHERY', 'NORTH TOUTLE HATCHRY',
        'KALAMA FALLS HATCHRY', 'MODROW TRAP', 'FALLERT CR HATCHERY',
        'LEWIS RIVER HATCHERY', 'WASHOUGAL HATCHERY', 'LYONS FERRY HATCHERY',
        'PRIEST RAPIDS HATCHERY', 'KENDALL CR HATCHERY',
        'MARBLEMOUNT HATCHERY', 'DUNGENESS HATCHERY', 'HURD CR HATCHERY',
        'MERWIN HATCHERY', 'MERWIN DAM FCF', 'SPEELYAI HATCHERY',
        'TUCANNON HATCHERY', 'EASTBANK HATCHERY', 'METHOW HATCHERY',
        'WHITEHORSE POND', 'SUNSET FALLS FCF', 'WALLACE R HATCHERY',
        'SOLDUC HATCHERY', 'WELLS HATCHERY', 'BAKER LK HATCHERY',
        'BINGHAM CR HATCHERY', 'SKOOKUMCHUCK HATCHRY', 'MCKERNAN HATCHERY',
        'REITER PONDS', 'ICY CR HATCHERY', 'BOGACHIEL HATCHERY',
        'SKAMANIA HATCHERY', 'OMAK HATCHERY', 'CHELAN HATCHERY',
        'SPOKANE HATCHERY', 'LK WHATCOM HATCHERY', 'BEAVER CR HATCHERY',
        'CHELAN PUD HATCHERY', 'DAYTON ACCLIMA. POND', 'COTTONWOOD CR POND',
        'CHIWAWA HATCHERY', 'GEORGE ADAMS HATCHERY', 'KALAMA FALLS HATCHERY',
        'TWISP ACCLIMATION PD', 'NORTH TOUTLE HATCHERY', 'PALMER HATCHERY',
        'COWLITZ SALMON', 'EELLS SPRINGS', 'RINGOLD SPRINGS', 'TUMWATER FALLS',
        'COWLITZ SALMON HATCH', 'RINGOLD SPRINGS HATCHE',
        'COWLITZ TROUT HATCH', 'WASHOUGAL RIVER FISH', 'SKOOKUMCHUCK HATCHER',
        'GEORGE ADAMS HATCHERY'
    ],
    'KING_HATCHERIES': [
        'CEDAR RIVER HATCHERY', 'ICY CR HATCHERY', 'ISSAQUAH',
        'SOOS CREEK HATCHERY', 'TOKUL CR HATCHERY'
    ]
}


# wrapper for queries
class Searcher:
    def __init__(self):
        a = Analyzer()
        self.data = a.results(False, False)

    def query(self, stat, breed, hatchery):
        t = TimeTracker()
        fq = FishQuery(self.data, stat, breed, hatchery, True)
        t.elapsedTime()
        return fq

    def queryBox(self, stat, breed, hList):
        t = TimeTracker()
        print('Querying... ' + str([stat, breed, hList]))
        qb = QueryBox(self.data, stat, breed,
                      HATCHERY_LISTS[hList])
        t.elapsedTime()
        return qb


# query object on JSON data
class FishQuery:
    def __init__(self, inputJSON, stat, breed, hatchery, numeric):
        self.inputJSON = inputJSON
        self.stat = stat.upper()
        self.breed = breed.upper()
        self.hatchery = hatchery.upper()
        self.numeric = numeric
        self.answer = self.result()
        # print(self.answer)

    # returns result of searching inputJSON based on query
    def result(self):
        data = []
        # print(self.hatchery)
        for b, rows in self.inputJSON.items():
            # print(b)
            if self.breed in b:
                # print(rows)
                for row in rows:
                    # print(row)
                    s = row[self.stat]
                    date = stringToDate(row['DATE'])
                    # print(s)
                    # print(date)
                    # print(self.hatchery)
                    if self.hatchery in row['FACILITY']:
                        if self.numeric:
                            if s == '-':
                                s = 0
                            else:
                                s = int(s.replace(',', ''))
                        data.append((date, s))
                data.sort(key=lambda tup: tup[0])
                # print(data)
        return ([(d[0]) for d in data], [d[1] for d in data])


# list of FishQuery's for Plotter.compare()
class QueryBox:
    def __init__(self, data, stat, breed, hList):
        self.queries = []
        for hatchery in hList:
            self.queries.append(FishQuery(data,
                                          stat, breed, hatchery, True))

    def allStats(self):
        return [q.stat for q in self.queries]

    def allBreeds(self):
        return [q.breed for q in self.queries]

    def allHatcheries(self):
        return [q.hatchery for q in self.queries]
