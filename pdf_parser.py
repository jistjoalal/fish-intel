from tabula import wrapper
from helpers import TimeTracker, YEARS


# area and column specs for tabula
AREA = [70, 0, 558, 775]

COLUMNS = {
    2016:      [21, 141.273, 225.423, 261.063, 295.603, 335.233, 370.873,
                411.543, 449.163, 487.773, 529.353, 559.053, 594.693, 625.343,
                665],

    2017:      [21, 141.273, 225.423, 261.063, 290.603, 335.233, 370.873,
                411.543, 449.163, 487.773, 529.353, 559.053, 594.693, 621.343,
                665],

    2018:      [21, 141.273, 225.423, 261.063, 295.603, 335.233, 370.873,
                411.543, 449.163, 487.773, 529.353, 559.053, 594.693, 621.343,
                665],

    'weird1':  [0, 80.6, 216.423, 253.063, 275.603, 331.233, 365.873,
                400.543, 440.163, 479.773, 515.353, 550.053, 582.693, 620.343,
                655],

    'weird2':  [0, 80.6, 216.423, 253.063, 275.603, 325.233, 365.873,
                400.543, 440.163, 479.773, 515.353, 550.053, 582.693, 620.343,
                655],

    'default': [0, 129.6, 221.423, 261.063, 290.603, 335.233, 370.873,
                411.543, 449.163, 487.773, 523.353, 559.053, 590.693, 625.343,
                662]
}


# converts all PDFs in given directory to JSON files
# runs tabula batch conversion to JSON, passing area and column specs
def pdfsToJSON(path, a, c):
    print('\nParsing all JSON files in ' + path + '\n')
    wrapper.convert_into_by_batch(path,
                                  output_format='json',
                                  guess=False,
                                  area=a,
                                  columns=c,
                                  pages='all',
                                  silent=True)


# runs tabula pdf conversion to JSON, passing area and column specs
def pdfToJSON(path, a, c):
    print('\nParsing ' + path + '\n')
    wrapper.convert_into(path, path[:-4] + '.json',
                         output_format='json',
                         guess=False,
                         area=a,
                         columns=c,
                         pages='all')


# parses all wdfw hatchery report JSON files
def parseMain():
    for yr in range(YEARS['start'], YEARS['end']):
        pdfsToJSON('data/hatchery_reports/' + str(yr) + '/',
                   AREA, COLUMNS.get(yr, COLUMNS.get('default')))


# hard code exceptions to normal parsing here
# only use if specifying date isn't enough
def parseAll():
    t = TimeTracker()
    parseMain()
    t.elapsedTime()


# parseAll()
