import matplotlib.pyplot as plt

from json_searcher import Searcher


# plots analyzed data based on FishQuery's and QueryBox's
class Plotter:

    # returns formatted list of hatcheries for one or more
    def pluralize(self, types):
        # duplicates
        types = list(set(types))
        # replace '' with 'All'
        for t in (types):
            t = 'All' if t == '' else t
        # print(types)
        # asdf vs. asdf vs. asdf
        if len((types)) <= 3:
            return ' vs. '.join((types))
        # asdf vs. asdf vs. asdf vs. ...
        elif len((types)) > 1:
            return ' vs. '.join(list(types)[:3]) + ' vs. ...'
        # asdf
        else:
            return types[0]

    # returns a formatted plot title based on:
    # - stat
    # - breed
    # - 1 or more hatcheries
    def fullTitle(self, stat, breed, hatchery):
        # make '' appear as 'All'
        hatchery = 'All' if hatchery == '' else hatchery
        # return title
        return ' - '.join([stat, breed, hatchery])

    # matplots 1 stat of breed from hatchery
    # time domain = YEARS or YEARS whenever data.json was made
    def plot(self, fq):
        # print(fq.hatchery)
        title = self.fullTitle(fq.stat, fq.breed, fq.hatchery)
        print('Plotting: ' + title + '...')

        # print(fq.hatchery)
        # data
        fishTable = fq.answer
        # print(fishTable)

        # annotations
        fig, ax = plt.subplots()
        ax.set_xlabel('Date')
        ax.set_ylabel(fq.stat)
        fig.suptitle(title, fontsize=14)

        # plot
        ax.scatter(fishTable[0], fishTable[1], label=title)
        ax.legend()

        # render
        plt.show()

    # matplots 1 stat of breed from hatchery
    # time domain = YEARS or YEARS whenever data.json was made
    # takes QueryBox as input
    def compare(self, qb):
        # queries
        fqs = qb.queries

        # data
        fishTable = []
        for fq in fqs:
            fishTable.append(fq.answer)
        # print(fishTable)

        # annotations
        stat = self.pluralize(qb.allStats())
        breed = self.pluralize(qb.allBreeds())
        hatchery = self.pluralize(qb.allHatcheries())
        fig, ax = plt.subplots()
        ax.set_xlabel('Date')
        ax.set_ylabel(stat)
        full = self.fullTitle(stat, breed, hatchery)
        fig.suptitle(full, fontsize=14)

        # plot
        for i in range(len(fqs)):
            ax.scatter(fishTable[i][0], fishTable[i][1],
                       label=self.fullTitle(fqs[i].stat,
                                            fqs[i].breed,
                                            fqs[i].hatchery))
        ax.legend()

        # render
        plt.show()


s = Searcher()
p = Plotter()

# fq = s.query('adult total', 'chinook', 'soos')
# p.plot(fq)


qb = s.queryBox('adult total', '', 'KING_HATCHERIES')
p.compare(qb)
