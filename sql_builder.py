import sqlite3
import sys
import json


def main():
    # buildDB()
    # grabData()
    pass


# returns json from file as hash
def loadJSON(file):
    with open(file) as f:
        return json.load(f)


def buildDB():
    fishData = loadJSON('data/analyzed_data.json')
    # print(fishData)

    conn = sqlite3.connect("data/fish_data.db")
    c = conn.cursor()
    s = ("insert into fish_data values "
         + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

    for fish in fishData:
        for row in fishData[fish]:
            sqlRow = list(row.values())
            sqlRow.insert(0, fish)
            print(sqlRow)
            c.execute(s, sqlRow)

    conn.commit()
    c.close()


def grabData():
    s = ("SELECT * FROM fish_data")

    c.execute(s)
    data = c.fetchall()

    adultTotals = []
    dates = []

    for row in data:
        adultTotals.append(row[3])
        dates.append(row[14])

    print(list(zip(adultTotals, dates)))


if __name__ == "__main__":
    main()
