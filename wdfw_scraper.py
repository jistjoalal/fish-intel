from datetime import timedelta, date
from helpers import successPercent, downloadFile, TimeTracker
from dup_remover import Remover


class FilenameGenerator:
    def __init__(self, start, end):
        self.start = start
        self.end = end

    # returns list of dates from 1 to 2
    def dateRange(self, date1, date2):
        for n in range(int((date2 - date1).days)+1):
            yield date1 + timedelta(n)

    def wdfwWeirdDate(self, date):
        return (str(date.year)[2:]
                + str(date.month).zfill(2)
                + str(date.day).zfill(2))

    def wdfwNormalDate(self, date):
        return (str(date.month).zfill(2)
                + str(date.day).zfill(2)
                + str(date.year)[2:])

    # all wdfw filenames w/ all weird naming conventions
    def allWdfwFilenames(self):
        filenames = []
        for dt in self.dateRange(self.start, self.end):
            if dt.year < 2010:
                filenames.append(str(dt.year) + '/e'
                                 + self.wdfwWeirdDate(dt) + '.pdf')
                filenames.append(str(dt.year) + '/e'
                                 + self.wdfwNormalDate(dt) + '.pdf')
            elif dt.year >= 2010:
                filenames.append(str(dt.year) + '/'
                                 + self.wdfwWeirdDate(dt) + '.pdf')
                filenames.append(str(dt.year) + '/'
                                 + self.wdfwNormalDate(dt) + '.pdf')
        return filenames


class Downloader:
    def __init__(self, start, end):
        self.fg = FilenameGenerator(start, end)
        self.count = 0
        self.success = 0

    def downloadReportByFileName(self, filename):
        # define url input and file output
        url = "https://wdfw.wa.gov/hatcheries/escapement/" + filename
        output = "data/hatchery_reports/" + filename

        # attempt download
        print('Trying ' + filename + '...\t', end='')
        if downloadFile(url, output):
            self.success += 1
        self.count += 1

    def downloadAll(self):
        t = TimeTracker()
        for f in self.fg.allWdfwFilenames():
            self.downloadReportByFileName(f)
        t.elapsedTime()

    def printStats(self):
        print('Attempted:\t' + str(self.count))
        print('Downloaded:\t' + str(self.success))
        print('Success Rate:\t' + successPercent(self.success, self.count))
        return self.success


start = date(2010, 1, 1)
end = date(2010, 1, 10)

d = Downloader(start, end)
d.downloadAll()
downloaded = d.printStats()

r = Remover()
r.removeHatcheryReportDups()
removed = r.printStats()

print('Total Reports:\t' + str(downloaded - removed))
